<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Car extends Model
{
    use SoftDeletes;
    protected $dates = ['deleted_at'];
    protected $fillable = ['model','quantity','price','brand_id'];

    public function brand()
    {
        return $this->belongsTo('App\Brand');
    }
}
