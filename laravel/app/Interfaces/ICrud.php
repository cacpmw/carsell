<?php

namespace App\Interfaces;

use Illuminate\Http\Request;

interface ICrud
{
    public function all();

    public function store(Request $r);

    public function update(Request $r,$id);

    public function destroy($id);

    public function find($id);
}
