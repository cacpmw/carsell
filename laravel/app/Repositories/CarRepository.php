<?php

namespace App\Repositories;

use App\Brand;
use Illuminate\Http\Request;
use App\Interfaces\ICrud;
use App\Car;

class CarRepository implements ICrud
{

    public function all()
    {
        return Car::all();

    }

    public function store(Request $r)
    {
        $newCar = Car::firstOrNew($r->validated());
        $newCar->save();

    }

    public function update(Request $r, $id)
    {
        Car::whereId($id)->update($r->validated());
    }

    public function destroy($id)
    {
        Car::destroy($id);
    }

    public function find($id)
    {
        return Car::findOrFail($id);
    }

    public function getBrands()
    {
        return Brand::orderBy('name', 'asc')->get();
    }
}
