<?php

namespace App\Http\Controllers\Auth;

use App\Role;
use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Auth\RegistersUsers;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        return Validator::make($data, [
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
            'role_id' => 'required|integer'
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        return User::create([
            'name' => $data['name'],
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
        ]);
    }

    public function showRegistrationForm()
    {
        try {
            $roles = Role::all();
            return view('auth.register', compact('roles', $roles));
        } catch (\Exception $ex) {
            $roles = null;
            return view('auth.register', compact('roles', $roles));
        }

    }

    public function register(Request $request)
    {
        try {

            $newUser = new User();
            $newUser->name = $request->get('name');
            $newUser->email = $request->get('email');
            $newUser->password = bcrypt($request->get('password'));
            $newUser->role_id = $request->get('role_id');
            $newUser->save();

            return redirect($this->redirectTo);
        } catch (\Exception $ex) {
            //todo Lookup why redirect to action is not working on 5.7
            //workaround for redirect to action
            $roles = Role::all();
            return view('auth.register', compact('roles', $roles));

        }


    }

}
