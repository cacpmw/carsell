<?php

namespace App\Http\Controllers;

use App\Brand;

use App\Http\Requests\StoreCar;
use App\Http\Requests\UpdateCar;


use App\Repositories\CarRepository as ICrud;

class CarController extends Controller
{
    protected $carRepository;

    public function __construct(ICrud $_carRepository)
    {
        $this->carRepository = $_carRepository;
        $this->middleware('auth');
        $this->middleware('admin')->except('index');
    }


    public function index()
    {
        try {

            $cars = $this->carRepository->all();
            return view('car.index', compact('cars', $cars));
        } catch (\Exception $ex) {
            $msg = __('exceptionMsg');
            return view('car.index')->with('msg', $msg);
        }

    }


    public function create()
    {
        try {
            $brands = $this->carRepository->getBrands();
            if (!count($brands) > 0) {
                $msg = __('noBrandsFound');
                return redirect()->action('HomeController@index')->with('msg', $msg);
            }
            return view('car.create', compact('brands', $brands));
        } catch (\Exception $ex) {
            $msg = __('exceptionMsg');
            return redirect()->action('CarController@index')->with('msg', $msg);
        }
    }


    public function store(StoreCar $request)
    {
        try {

            $this->carRepository->store($request);
            return redirect()->action('CarController@index');
        } catch (\Exception $ex) {
            $msg = __('exceptionMsg');
            return redirect()->action('CarController@index')->with('msg', $msg);
        }

    }

    public function edit($id)
    {
        try {
            $car = $this->carRepository->find($id);
            $brands = $this->carRepository->getBrands();
            if (!count($brands) > 0) {
                $msg = __('noBrandsFound');
                return redirect()->action('HomeController@index')->with('msg', $msg);
            }
            return view('car.edit')->with('car', $car)->with('brands', $brands);
        } catch (\Exception $ex) {
            $msg = __('exceptionMsg');
            return redirect()->action('CarController@index')->with('msg', $msg);
        }
    }


    public function update(UpdateCar $request, $id)
    {
        try {

            $this->carRepository->update($request, $id);
            return redirect()->action('CarController@index');
        } catch (\Exception $ex) {
            $msg = __('exceptionMsg');
            return redirect()->action('CarController@index')->with('msg', $msg);
        }

    }

    public function destroy($id)
    {
        try {

            $this->carRepository->destroy($id);
            return redirect()->action('CarController@index');

        } catch (\Exception $ex) {
            $msg = __('exceptionMsg');
            return redirect()->action('CarController@index')->with('msg', $msg);
        }
    }
}
