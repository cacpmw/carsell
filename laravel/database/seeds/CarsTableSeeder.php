<?php

use Illuminate\Database\Seeder;

class CarsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('cars')->insert([
            'model' => 'BMW Model 1',
            'quantity' => 2,
            'price' => 245000,
            'brand_id' => 1

        ]);
        DB::table('cars')->insert([
            'model' => 'Audi Model 1',
            'quantity' => 3,
            'price' => 120000,
            'brand_id' => 2

        ]);
        DB::table('cars')->insert([
            'model' => 'Mercedez Model 1',
            'quantity' => 2,
            'price' => 300000,
            'brand_id' => 3

        ]);
    }
}
