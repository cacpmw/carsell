<?php
return [
    'Password' => 'Senha',
    'Email' => 'Email',
    'Login' => 'Login',
    'RememberMe' => 'Lembrar-se de mim',
    'ForgotYourPassword' => 'Esqueceu sua senha?',
    'Register' => 'Registrar-se',
    'Name' => 'Nome',
    'ConfirmPassword' => 'Confirmar Senha',
    'Role' => 'Perfil',
    'ListOfCars' => 'Lista de Carros',
    'Cars' => 'Carros',
    'New' => 'Novo',
    'List' => 'Lista',
    'Logout' => 'Sair',
    'Model' => 'Modelo',
    'Quantity' => 'Quantidade',
    'Brand' => 'Marca',
    'Price' => 'Preço',
    'Actions' => 'Ações',
    'Filter' => 'Filtrar',
    'Edit' => 'Editar',
    'Delete' => 'Deletar',
    'Save'=>'Salvar'
];
