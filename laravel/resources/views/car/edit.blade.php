@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">{{ __('Editar') }}</div>

        <div class="card-body">
            @include('partials.car._editForm')
        </div>
    </div>
@endsection
