@extends('layouts.app')
@section('content')
    <div class="card">
        <div class="card-header">{{ __('labels.New') }}</div>

        <div class="card-body">
            @include('partials.car._createForm')
        </div>
    </div>
@endsection
