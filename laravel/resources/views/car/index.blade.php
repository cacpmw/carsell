@extends('layouts.app')
@section('content')
    @if(session('msg'))
        <div class="alert alert-danger">
            <p>{{session('msg')}}</p>
        </div>
    @elseif(count($cars)>0)

        @include('partials.car._indexTable')
        @include('partials.car._indexTableFilterScript')

    @else
        <div class="text-center">
            <p>{{__('noRecordsFound')}}</p>
        </div>
    @endif
@endsection


