<form method="POST" action="{{ route('cars.update',$car->id) }}">
    @method('PUT')
    @csrf

    <div class="form-group row">
        <label for="model" class="col-md-4 col-form-label text-md-right">{{ __('Modelo') }}</label>

        <div class="col-md-6">
            <input id="model" type="text"
                   class="form-control{{ $errors->has('model') ? ' is-invalid' : '' }}"
                   name="model" value="{{$car->model }}" required autofocus>

            @if ($errors->has('model'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('model') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="quantity" class="col-md-4 col-form-label text-md-right">{{ __('Quantidade') }}</label>

        <div class="col-md-6">
            <input id="quantity" type="number" step="0"
                   class="form-control{{ $errors->has('quantity') ? ' is-invalid' : '' }}" name="quantity"
                   value="{{$car->quantity }}" required>

            @if ($errors->has('quantity'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('quantity') }}</strong>
                                    </span>
            @endif
        </div>
    </div>

    <div class="form-group row">
        <label for="price" class="col-md-4 col-form-label text-md-right">{{ __('Preço') }}</label>

        <div class="col-md-6">
            <input id="price" type="number" step="0.01"
                   class="form-control{{ $errors->has('price') ? ' is-invalid' : '' }}" name="price"
                   value="{{$car->price }}" required>

            @if ($errors->has('price'))
                <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('price') }}</strong>
                                    </span>
            @endif
        </div>
    </div>
    @if(isset($brands))
        <div class="form-group row">
            <label for="role" class="col-md-4 col-form-label text-md-right">{{ __('Role') }}</label>

            <div class="col-md-6">
                <select required name="brand_id" type="number" id="brand_id"
                        class="form-control{{ $errors->has('brand_id') ? ' is-invalid' : '' }}">
                    @foreach($brands as $brand)
                        @if($brand->id==$car->brand_id)
                            <option selected value="{{$brand->id}}">{{$brand->name}}</option>
                        @else
                            <option value="{{$brand->id}}">{{$brand->name}}</option>
                        @endif
                    @endforeach
                </select>

                @if ($errors->has('brand_id'))
                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $errors->first('brand_id') }}</strong>
                                    </span>
                @endif
            </div>
        </div>
    @endif
    <div class="form-group row mb-0">
        <div class="col-md-6 offset-md-4">
            <button type="submit" class="btn btn-primary">
                {{ __('Atualizar') }}
            </button>
        </div>
    </div>
</form>
