<div class="row">
    <input id="term" onkeyup="filterTable()" name="term" autofocus placeholder="{{__('labels.Filter')}}..."
           type="text"
           class="form-control">
</div>
<br>
<div class="row">
    <table id="table" class="table table-sm  table-condensed table-hover">
        <caption>{{__('labels.ListOfCars')}}</caption>
        <thead class="thead-dark">
        <tr>
            <th>#</th>
            <th>{{__('labels.Model')}}</th>
            <th>{{__('labels.Quantity')}}</th>
            <th>{{__('labels.Brand')}}</th>
            <th>{{__('labels.Price')}}</th>
            @if(Auth::user()->isAdmin())
                <th>{{__('labels.Actions')}}</th>
            @endif
        </tr>
        </thead>
        <tbody>
        @foreach($cars as $car)
            <tr>
                <td>{{$car->id}}</td>
                <td>{{ $car->model }}</td>
                <td>{{ $car->quantity }}</td>
                <td>{{ $car->brand->name }}</td>
                <td>R$ {{ $car->price }}</td>
                @if(Auth::user()->isAdmin())
                    <td>
                        <a href="{{route('cars.edit',$car->id)}}">
                            <button class="btn btn-sm btn-primary" type="button">{{__('labels.Edit')}}</button>
                        </a>
                        <form method="POST" action="{{route('cars.destroy',$car->id)}}">
                            @method('DELETE')
                            @csrf
                            <button class="btn btn-sm btn-danger" type="submit">{{__('labels.Delete')}}</button>
                        </form>
                    </td>
                @endif
            </tr>
        @endforeach
        </tbody>
    </table>
</div>
