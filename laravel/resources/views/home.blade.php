@extends('layouts.app')

@section('content')
    @if(session('msg'))
        <div class="col-md-8 offset-2">
            <div class="alert alert-danger">
                <p>{{session('msg')}}</p>
            </div>
        </div>
    @endif

    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    {{__('youAreLoggedIn',['Name'=>Auth::user()->name])}}
                </div>
            </div>
        </div>
    </div>

@endsection
